; Макросы для номеров системных вызовов Unix
%define SYS_EXIT 60
%define SYS_WRITE 1
%define SYS_READ 0
; Конец номеров системных вызовов
; Макросы для дескрипторов стандартных потоков
%define stdin 0
%define stdout 1
%define stderr 2
; Конец дескрипторов стандартных потоков

section .text 

; Принимает код возврата через rdi и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall 

; Принимает указатель на нуль-терминированную строку через rdi, возвращает её длину
string_length:
    mov rax, -1 ; rax = -1
.loop:
    inc rax
    cmp byte[rdi + rax], 0
    jne .loop
    ret

; Принимает указатель на нуль-терминированную строку через rdi, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax ; rdx = length of string
    mov rsi, rdi
    mov rdi, stdout
    mov rax, SYS_WRITE
    syscall
    ret

; Принимает код символа через rdi и выводит его в stdout
print_char:
    push di
    mov rdi, rsp
    call print_string
    pop di
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число из rdi в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rsi, rsp
    sub rsp, 24
    call format_uint
    mov rdi, rsi
    call print_string
    add rsp, 24
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge print_uint
    neg rdi
    mov rsi, rsp
    sub rsp, 24
    call format_uint
    dec rsi
    mov byte[rsi], '-'
    mov rdi, rsi
    call print_string
    add rsp, 24
    ret

; Форматирует беззнаковое 8-байтовое число из rdi в строку,
; на конец которой указывает rsi.
; После выполнения rsi указывает на начало строки.
format_uint:
    mov r8, 10
    mov rax, rdi
    dec rsi
    mov byte[rsi], 0 ; положим в конец нулль-терминатор
.loop:
    mov rdx, 0  ; обнулим остаток от деления
    div r8      ; делим на 10
    add dl, '0' ; остатку придаём смещение от символа '0' в ASCII
    dec rsi
    mov byte[rsi], dl
    test rax, rax
    jnz .loop
    ret

; Принимает два указателя rdi и rsi на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rsi 
    push rdi
    call string_length  
    mov r8, rax        ; first string's length -> r8
    pop r9             ; first string  -> r9
    mov rdi, [rsp]     ; second string -> rdi
    call string_length 
    cmp rax, r8        ; cmp second string's length to first string's length
    jne .false
    pop r8             ; second string -> r8
.qcomp:
    cmp rax, 8
    jl .bcomp
    mov rdi, [r8]
    cmp rdi, [r9]
    jne .false
    sub rax, 8
    add r9, 8
    add r8, 8
    jmp .qcomp
.bcomp:
    cmp rax, 0
    jl .true
    mov dil, byte[r8]
    cmp dil, byte[r9]
    jne .false
    dec rax
    inc r8
    inc r9
    jmp .bcomp
.true:
    mov rax, 1
    ret
.false:
    pop r8              ; clear rsi from the top of the stack
    mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push qword 0 ; Выделим место под прочитанный символ
    mov rdi, stdin
    mov rsi, rsp
    mov rdx, 1
    mov rax, SYS_READ
    syscall
    test rax, rax
    jne .success
.fail:
    mov qword[rsp], 0 ; Вернём ноль, если не успешно 
.success:    
    pop rax
    ret

; Принимает: адрес начала буфера в rdi, размер буфера в rsi
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rdi
    mov r8, rdi ; спасём от затирания при вызове read_char
    mov r9, rsi
.skip:
    call read_char
    cmp al, 0
    je .fail
    cmp al, 0x20
    je .skip
    cmp al, 0x9
    je .skip
    cmp al, 0xA
    je .skip
.loop:
    dec r9
    jz .fail ; нету места для нулль-терминатора
    mov byte[r8], al ; помещаем только что прочитанный символ
    inc r8
    call read_char
    test al, al
    jz .success
    cmp al, 0x20
    je .success
    cmp al, 0x9
    je .success
    cmp al, 0xA
    je .success
    jmp .loop
.success:
    mov byte[r8], 0
    pop rax
    mov rdx, r8
    sub rdx, rax
    ret
.fail:
    pop rdi
    mov rax, 0
    mov rdx, 0
    ret
    
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor     rdx, rdx
    xor     rax, rax
.loop:
    xor     rcx, rcx
    mov     cl, byte[rdi]
    sub     cl, '0'
    cmp     cl, 0
    jl      .end
    cmp     cl, 9
    jg      .end
    imul    rax, rax, 10
    add     rax, rcx
    inc     rdi
    inc     rdx
    jmp     .loop
.end:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov cl, byte[rdi]
    cmp cl, '-'
    jne parse_uint
    inc rdi
    call parse_uint
    test rdx, rdx 
    jz .end
    inc rdx ; +1 символ за знак 
    neg rax
.end:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi: указатель на строку
; rsi: указатель на буфер
; rdx: длина буфера
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rdx, rax ; длина строки не учитывает нулль-терминатор -> rdx > rax
    jg .copy
.fail:
    mov rax, 0 ; 
    ret
.copy:
    push rax ; сохраним длину строки чтобы потом вернуть
    inc rax  ; учтём наличие нулль-терминатора
.qcpy:
    cmp rax, 8 ; Если в нескопированной части строки осталось меньше 8 байт
    jl .bcpy   ; То копируем побайтово, а не целыми 64-битными словами
    mov r8, [rdi]
    mov [rsi], r8
    add rdi, 8
    add rsi, 8
    sub rax, 8
    jmp .qcpy
.bcpy:
    test rax, rax
    je .end
    mov r8b, byte[rdi]
    mov byte[rsi], r8b
    inc rdi
    inc rsi
    dec rax
    jmp .bcpy
.end:
    pop rax ; достанем сохранённую длину строки со стека
    ret
